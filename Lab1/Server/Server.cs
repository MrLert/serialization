﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace Lab1.Server
{
    public class Server : IServer
    {
        public string port;
        private readonly JsonSerializer jsonSerializer = new JsonSerializer();
        private readonly HttpListener httpListener = new HttpListener();
        private Output output = null;
        private Input input = null;
        private readonly string[] nameMethods = {"Ping", "GetAnswer", "PostInputData", "Stop"};
        private readonly Dictionary<string, MethodInfo> Methods = new Dictionary<string, MethodInfo>();

        public Server(string port)
        {
            this.port = port;
            foreach (string method in nameMethods)
            {
                httpListener.Prefixes.Add($"http://127.0.0.1:{port}/{method}/");
                Methods[method] = typeof(Server).GetMethod(method);
            }
            
            Method();
        }

        public void Method()
        {
            httpListener.Start();
            while (true)
            {
                var request = httpListener.GetContext();
                foreach (var method in nameMethods)
                {
                    if (request.Request.RawUrl.Contains(method))
                    {
                        Methods[method].Invoke(this, new object[] {request});
                        if (method == "Stop")
                            return;
                        break;
                    }
                }
            }
        }

        public void Ping(HttpListenerContext textContext)
        {
            textContext.Response.StatusCode = (int)HttpStatusCode.OK;
            var response = textContext.Response.OutputStream;
            using (var stream = new StreamWriter(response))
            {
                stream.Write("");
            }
        }

        public void PostInputData(HttpListenerContext textContext)
        {
            string obj;
            var request = textContext.Request.InputStream;
            using (var stream = new StreamReader(request))
            {
               obj = stream.ReadToEnd();
            }
            input = jsonSerializer.Deserialize<Input>(obj);
            Outobj();

            var response = textContext.Response.OutputStream;
            using (var stream = new StreamWriter(response))
            {
                stream.Write("");
            }
        }

        public void GetAnswer(HttpListenerContext textContext)
        {
            var data = jsonSerializer.Serializer(output).Replace(Environment.NewLine, "").Replace(" ", "").Replace("\t", "");
            var response = textContext.Response.OutputStream;
            using (var stream = new StreamWriter(response))
            {
                stream.Write(data);
            }
        }

        public void Stop(HttpListenerContext textContext)
        {
            var response = textContext.Response.OutputStream;
            using (var stream = new StreamWriter(response))
            {
                stream.Write("");
            }
            httpListener.Stop();
        }

        public void Outobj()
        {
            decimal sumR = 0;
            foreach (var i in input.Sums)
            {
                sumR += i;
            }
            sumR *= input.K;
            var mulR = 1;
            foreach (var i in input.Muls)
            {
                mulR *= i;
            }
            var mas = input.Sums.ToList();
            foreach (var i in input.Muls)
            {
                mas.Add(i);
            }
            var sortedI = mas.OrderBy(x => x).ToArray();
            output = new Output() { SumResult = sumR, MulResult = mulR, SortedInputs = sortedI };
        }        
    }
}