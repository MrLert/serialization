﻿using System.Net;

namespace Lab1.Server
{
    public interface IServer
    {
        void Ping(HttpListenerContext textContext);
        void PostInputData(HttpListenerContext textContext);
        void GetAnswer(HttpListenerContext textContext);
        void Stop(HttpListenerContext textContext);
    }
}